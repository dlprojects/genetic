from tqdm import tqdm_notebook as tqdm
from matplotlib.pyplot import plot

class ES:
    def __init__(self, population_size):
        self._population_size = population_size
        self._history = []
        self._generation = 0
        
    def __call__(self, generations=1, num_workers=None):
        for _ in tqdm(range(generations)):
            self._population, fitness = self._strategy(num_workers)
            self._generation += 1
            self._history.append((self._generation, fitness))
            print(f'{fitness:.2f}', end='\r')
            
    def _strategy(self):
        pass
    
    def solution(self):
        pass
    
    def plot_history(self):
        plot(*zip(*self._history))

class SimpleES(ES):
    def __init__(self, evaluator, population_size, mean, std=1):
        super().__init__(population_size)
        self._evaluator = evaluator
        self._mean = mean
        self._std = std
        self._population = [mean + torch.randn(len(mean)) * std 
                            for _ in range(population_size)]
        self._history.append((0, evaluator(mean)))
    
    def _strategy(self, num_workers):
        with ProcessPoolExecutor(num_workers) as executor: 
            fitnesses = list(executor.map(self._evaluator, self._population))
        
        max_fitness_id = np.argmax(fitnesses)
        self._mean = self._population[max_fitness_id]
        fitness = fitnesses[max_fitness_id]
        
        new_population = [self._mean + torch.randn(len(self._mean)) * self._std 
                          for _ in range(self._population_size)]
        
        return new_population, fitness
        
    def solution(self):
        return self._mean

class SimpleGA(ES):
    def __init__(self, evaluator, population_size, mean, surviving_fraction=0.1, std=1e-3):
        super().__init__(population_size)
        self._evaluator = evaluator
        self._surviving_size = max(2, int(surviving_fraction * population_size))
        self._std = std
        self._population = [mean + torch.randn(len(mean)) 
                            for _ in range(population_size)]
        self._history.append((0, evaluator(mean)))
        
    def _strategy(self, num_workers):
        with ProcessPoolExecutor(num_workers) as executor: 
            fitnesses = list(executor.map(self._evaluator, self._population))
            
        fitness_ids = np.argsort(fitnesses)
        surviving_ids = fitness_ids[-self._surviving_size:]
        survivors = [self._population[i] for i in surviving_ids]
        
        def _breed(father, mother):
            mask = torch.bernoulli(torch.ones(len(father)) * 0.5).float()
            noise = torch.randn(len(father)) * self._std
            
            return father * mask + mother * (1 - mask) + noise
        
        def _random_choice(a, size, replace=True, p=None):
            ids = np.random.choice(np.arange(len(a)), size, replace, p)
            return [a[i] for i in ids]
        
        breeding_mates = [_random_choice(survivors, 2, replace=False)
                          for _ in range(len(self._population) - len(survivors))]
        children = [_breed(*mates) for mates in breeding_mates]
        
        new_population = survivors + children
        
        fitness = fitnesses[surviving_ids[-1]]
        self._solution = survivors[-1]
        
        return new_population, fitness
    
    def solution(self):
        return self._solution

class CmaES(ES):
    def __init__(self, evaluator, population_size, mean, surviving_fraction=0.25):
        super().__init__(population_size)
        self._evaluator = evaluator
        self._surviving_size = max(2, int(surviving_fraction * population_size))
            
        self._mean = mean
        self._population = [mean + torch.randn(len(mean)) 
                            for _ in range(population_size)]
        self._history.append((0, evaluator(mean)))
        
    def _strategy(self, num_workers):
        with ProcessPoolExecutor(num_workers) as executor: 
            fitnesses = list(executor.map(self._evaluator, self._population))
            
        fitness_ids = np.argsort(fitnesses)
        surviving_ids = fitness_ids[-self._surviving_size:]
        survivors = torch.stack([self._population[i] for i in surviving_ids])
        
        def cov(x, m):
            p = x - m
            return (p.unsqueeze(-1) * p.unsqueeze(1)).mean(0)
        
        self._cov = cov(survivors, self._mean)
        self._mean = survivors.mean(0)
        
        def _multivariate(mu, sigma):
            l = torch.from_numpy(np.linalg.cholesky(sigma.numpy()))
            return list(torch.mm(torch.randn(self._population_size - self._surviving_size,
                                             len(mu)), l))
        
        new_population = _multivariate(self._mean, self._cov) + list(survivors)
        
        fitness = fitnesses[surviving_ids[-1]]
        self._solution = survivors[-1]
        
        return new_population, fitness
    
    def solution(self):
        return self._solution