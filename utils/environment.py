import numpy as np

def add_energy(env, null_actions=None):
    _step = env.step
    if type(null_actions) is not list: null_actions = [null_actions]
        
    def new_step(action):
        observation, reward, done, info = _step(action)
        energy = float(action not in null_actions) if null_actions[0] is not None else 0
        return observation, reward, done, info, energy
    env.step = new_step

def make_specs(env):
    env.spec.n_obs = int(np.prod(env.observation_space.shape))
    env.spec.n_act = env.action_space.n


def run_env(env, action_fn=None, episodes=1, steps=np.inf, render=False):
    if action_fn is None: action_fn = lambda x: env.action_space.sample()
    num_steps, rewards, energies = 0, 0, 0
    for _ in range(episodes):
        observation = env.reset()
        done = False
        while not done:
            if render: env.render()
            observation, reward, done, info, energy = env.step(action_fn(observation))
            rewards += reward
            energies += energy
            num_steps += 1
            if num_steps >= steps: break
            
    return rewards, energies