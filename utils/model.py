import numpy as np

import torch
from torch.nn import functional as F
from torch.autograd import Variable

def get_params(model):
    parameters = torch.cat([p.data.view(-1) for p in model.parameters() if p.requires_grad])
    return parameters

def set_params(model, params):
    param_idx = 0
    for p in model.parameters():
        if not p.requires_grad: continue
        n_params = torch.prod(torch.IntTensor([p.size()]))
        new_param = params[param_idx: param_idx + n_params].view(p.size())
        p.data = new_param
        param_idx += n_params

def get_action(model, observation, probabilistic=False):
    observation = Variable(torch.from_numpy(observation).float(), volatile=True)
    actions = model(observation.unsqueeze(0))[0]
    
    if probabilistic:
        actions = F.softmax(actions, dim=0).data.numpy()
        action = np.random.choice(np.arange(len(actions)), p=actions)
    else:
        action = actions.data.numpy().argmax()
    
    return action