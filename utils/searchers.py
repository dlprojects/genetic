from matplotlib.pyplot import plot
from time import time

from utils.model import get_params, set_params

class Searcher:
    def __init__(self, model, evaluator):
        self._model = model
        self._evaluator = evaluator
        
        self._params = get_params(model)
        self._rewards = [(0, evaluator(model))]
        self._steps = 0
        
    def __call__(self, timeout=60):
        start_time = time()
        while True:
            new_params = self._new_params()
            
            set_params(self._model, new_params)
            reward = self._evaluator(self._model)
            
            if reward > self._rewards[-1][1]:
                self._steps += 1
                self._rewards.append((self._steps, reward))
                self._params = new_params
                print(f'{reward:.2f}', end='\r')
                self._successful()
            else:
                self._unsuccessful()
                
            if time() - start_time > timeout: self.finish(); break
    
    def _new_params(self):
        pass
    
    def _successful(self):
        self._steps += 1
    
    def _unsuccessful(self):
        self._steps += 1
    
    def finish(self):
        set_params(self._model, self._params)
        
    def plot_search(self):
        plot(*zip(*self._rewards))

class RandomSearcher(Searcher):
    def __init__(self, model, evaluator, std=1):
        super().__init__(model, evaluator)
        self._std = std
    
    def _new_params(self):
        return torch.randn(len(self._params)) * self._std


class SprialSearcher(Searcher):
    def __init__(self, model, evaluator, std_range=(-3, 2), std_growth=-2):
        super().__init__(model, evaluator)
        self._std_range = std_range
        self._std_growth = 10 ** std_growth
        self._std = 10 ** std_range[0]
        self._direction = 1
        
    def _new_params(self):
        noise = torch.randn(len(self._params)) * self._std
        return self._params + noise
    
    def _successful(self):
        super()._successful()
        self._std = 10 ** self._std_range[0]
        
    def _unsuccessful(self):
        super()._unsuccessful()
        self._std *= 1 + self._direction * self._std_growth
        print(f'{self._rewards[-1][1]:.2f}, {self._std:.2e}', end='\r')
        
        std_mag = np.log10(self._std)
        is_below = (std_mag < self._std_range[0] and self._direction < 0)
        is_above = (std_mag > self._std_range[1] and self._direction > 0)
        if is_above or is_below: self._direction *= -1